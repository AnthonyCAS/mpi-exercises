#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define val 11

int main(int argc, char* argv[]) {
  int id_proceso,procesos;
  MPI_Status estado;
  MPI_Request request; // par ala comunicacion no bloqueante

  int flag,myfound, nvalues;
  int array[300];
  int *sub; // temporal subarrays, 
  int i,j,dummy,index;

  FILE *infile;

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&id_proceso);
  MPI_Comm_size(MPI_COMM_WORLD,&procesos);

  myfound=0;
  if (id_proceso==0) {
    infile=fopen("array.data","r");
    for(i=0;i<300;++i) {
      fscanf(infile,"%d",&array[i]);
    }
  }
  nvalues=300/procesos;
  sub = (int *)malloc(nvalues *sizeof(int));

  MPI_Scatter(array,nvalues,MPI_INT,sub,nvalues,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD);  /* esperamos que todos los procesos pasen*/

  MPI_Irecv(&dummy,1,MPI_INT,MPI_ANY_SOURCE,86,MPI_COMM_WORLD,&request);
  MPI_Test(&request,&flag,&estado);
  i=0;
  while(!flag && i<nvalues) {
    if (sub[i]==val) {
      dummy=123;
      //for(j=0;j<=procesos;++j) { 
         MPI_Send(&dummy,1,MPI_INT,0,86,MPI_COMM_WORLD);
      //}
      printf("P:%d lo encontro en la pos: %d\n",id_proceso,i);
      myfound=1;
    }
    MPI_Test(&request,&flag,&estado);
    ++i;
  }
  if (!myfound) {
    if (i==0) 
      index=0;
    else
      index=i-1;
    printf("P:%d paro en la pos: %d\n",id_proceso,index);
  }
 MPI_Finalize(); 
 return 0;
}

