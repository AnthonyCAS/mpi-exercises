#include "mpi.h" 
#include <math.h> 
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define lanzamientos     1000000   
double num_ram(double a, double b) {
  return  ((b - a) * ((double) rand()/(double) RAND_MAX)) + a;
} 

int main(int argc, char *argv[]) { 

    int id_proceso, procesos, i, aciertos, limite_low,limite_up;  
    double x, y, pi, sum, aux;
 
    limite_low   = -1;
    limite_up   = 1;
    aciertos = 0;

    MPI_Init(&argc,&argv); 
    MPI_Comm_size(MPI_COMM_WORLD,&procesos); 
    MPI_Comm_rank(MPI_COMM_WORLD,&id_proceso); 
    
    srand(time(NULL));
        //proceso padre
        if (id_proceso == 0) { 
            printf("\n\n    Numero de Procesos : %d\n", procesos);
            printf("    Lanzamientos : %d\n\n", lanzamientos); 
        } 

      /* lanzamientos*/
       for (i = 1; i <= lanzamientos; ++i) {
            x = num_ram(limite_low, limite_up);
            y = num_ram(limite_low, limite_up);
         
            aux = pow(x, 2) + pow(y, 2);
         
            if (aux <= 1.0) {
               aciertos++;
            }
     
      } 
 
      pi = 4.0 * (double)aciertos/(double)lanzamientos;
      MPI_Reduce(&pi, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
 
        if (id_proceso == 0) {
            sum = sum / procesos;
            printf("    Valor de PI  : %11.10f\n", sum);
        }

      MPI_Finalize();
      return 0;
}
