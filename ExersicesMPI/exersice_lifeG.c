#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

void *mi_malloc ( int tam);
void leer_cells(FILE *file, int *m, int *n, int *g);
void imprime (int n, int m);
int esta_viva (int x, int y, int m);
int esta_vacia (int x, int y, int m);
int busca (int x, int y, int desde, int hasta, int *temp);
int *copia_gen (int size, int *gen);
int vecinos (int x, int y, int m, int n);
int *vida (int n, int m, int desde, int hasta, int *viven);
int *nacer (int n, int m, int desde, int hasta, int *nacen);
int *unifica (int nv1, int nv2, int *nfinal, int *v1, int *v2);
int cmp (int nv1, int nv2, int *v1, int *v2);

FILE *file;
int *generacion, *old_generation = NULL, *new_generation = NULL, send = 0;
int id_proceso, llamada;

int main(int argc, char *argv[]) {
    int  n, m,g, procesos , i, j, k, count, mant, inicio, fin, tam;
    double startwtime, endwtime, swimprime, ewimprime, totalimprime = 0, esclavo = 0, total_esclavo = 0, startrans, endtrans, trans = 0;
    char entrada[40];
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int namelen,source, dest, tag = 100, resto,
        *nacen,
	*viven,
	*temp,
	*temp1,
	*temp2,
        nacieron,
	vive,
	llegadas,
	mtemp,
        iguales = 0,
	p,
	intervalo[2];
    MPI_Status status, status1;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&procesos);
    MPI_Comm_rank(MPI_COMM_WORLD,&id_proceso);
    MPI_Get_processor_name(processor_name,&namelen);
     
	if(id_proceso == 0) {
		
		file = fopen ("celula1.txt", "r");
        leer_cells (file, &m, &n, &g);

		MPI_Bcast( &n, 1, MPI_INT, 0, MPI_COMM_WORLD);
		count = 0;
		send = 0;
		startwtime = MPI_Wtime();
		/*printf ("Generación %d m = %d\n", count, m);
		printf ("Generación %d n = %d\n", count, n);
		for(i = 0;i<2*m;i++)
			printf (" %d ", generacion[i]);*/
	    do {
			printf ("Generación %d m = %d\n", count, m);
			//m numero de inicial de celdas vivas, n size tablero
			imprime (n, m);

			if (m < (procesos - 1)) {
				p = m;
			} 
			else {
				p = (procesos - 1);
			}

			if (m > 0) {
				tam = m / p;
				resto = m % p;
			} 
			else {
				tam = resto = 0;
			}
			
			for (dest = 1; dest < procesos; dest++) {
		   		
				if ((dest > m) || ( m < 0)) {
					inicio = fin = -2;
				} else {
		   			if ( (dest - 1) < resto) {
			   			inicio = (tam + 1) * (dest -1);
			   			fin = inicio + (tam + 1);
		   			} else {
			   			inicio = (tam * (dest - 1)) + resto;
			   			fin = inicio + tam;
		   			}
				}
				
				intervalo[0] = inicio;
				intervalo[1] = fin;
				startrans = MPI_Wtime ();
				MPI_Send( intervalo, 2, MPI_INT, dest, tag, MPI_COMM_WORLD);
				endtrans = MPI_Wtime ();
		   		trans += (endtrans - startrans);
				
			}

			startrans = MPI_Wtime ();
			MPI_Bcast( &m, 1, MPI_INT, 0, MPI_COMM_WORLD);
			endtrans = MPI_Wtime ();
		   	trans += (endtrans - startrans);
			
			if (m > 0) {
				startrans = MPI_Wtime ();
				MPI_Bcast( generacion, m * 2, MPI_INT, 0, MPI_COMM_WORLD);
				endtrans = MPI_Wtime ();
		   		trans += (endtrans - startrans);
				if (old_generation != NULL) {
					free (old_generation);
				}
                old_generation = copia_gen ( m, generacion);
				mant = m;
			} 

			else {
				mant = 0;
				if (old_generation != NULL) {
					free (old_generation);
				}
				old_generation = NULL;
			}
			
			
			temp1 = NULL;
			temp2 = NULL;
			nacen = 0;
			llegadas = 0;
			////MPI_Barrier ( MPI_COMM_WORLD );
			for (dest = 1; dest < procesos; dest++) {
				startrans = MPI_Wtime ();
				MPI_Recv( &vive, 1, MPI_INT,MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
				endtrans = MPI_Wtime ();
		   		trans += (endtrans - startrans);
				if (vive > 0) {
					temp = (int *) mi_malloc ((vive * 2) * sizeof(int));
					startrans = MPI_Wtime ();
					MPI_Recv( temp, (vive * 2), MPI_INT, status.MPI_SOURCE, tag, MPI_COMM_WORLD, &status1);
					endtrans = MPI_Wtime ();
		   			trans += (endtrans - startrans);
					temp1 = unifica ( vive, llegadas, &mtemp, temp, temp2);
					
					if (llegadas > 0) {
						free (temp2);
					}
					temp2 = copia_gen (mtemp, temp1);
					llegadas = mtemp;
					free ( temp );
					if ( mtemp > 0) {
						free ( temp1 );
					}
				}
			}

			if (m > 0) {
				free ( generacion );
			}

			if (llegadas > 0) {
				generacion = copia_gen ( llegadas, temp2);
				m = llegadas;
				free ( temp2 );
			} 
			else {
				m = 0;
				
			}
			
			send++;
			count++;
			/*if ( count == g) {
				send = -1;
				//printf ("Maximo de Generaciones\n");
				
				
			} else {

				/*if (cmp ( mant, m, old_generation, generacion)) {
					if (!iguales) {
						iguales = 1;
					} else {	
						count = g;
						//printf ("Dos generaciones iguales\n");
						
						
						
					}
				} else {
					
						iguales = 0;
				}*/
			//}
			
		} while ( count < g);


        
        if (m > 0) {
			free( generacion);
		}
		if (mant > 0) {
                	free( old_generation);
		}

		for (i = 1; i < procesos; i++) {
			intervalo[0] = -1;
			intervalo[1] = -1;
			startrans = MPI_Wtime ();
			MPI_Send( intervalo, 2, MPI_INT, i, tag, MPI_COMM_WORLD);
			endtrans = MPI_Wtime ();
		   		trans += (endtrans - startrans);	
		}
			m = -1;
			startrans = MPI_Wtime ();
			MPI_Bcast( &m, 1, MPI_INT, 0, MPI_COMM_WORLD);		
			endtrans = MPI_Wtime ();
		   	trans += (endtrans - startrans);
			//MPI_Barrier ( MPI_COMM_WORLD );
		
		
		endwtime = MPI_Wtime();
		MPI_Reduce(&esclavo, &total_esclavo, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		printf ("Tiempo total es %f\n", endwtime - startwtime );
		printf ("Tiempo de Transmisiones %f\n", trans);
		printf ("Tiempo sin Transmisiones %f\n", (endwtime - startwtime) - trans);
		printf ("Tiempo total de computo %f\n", total_esclavo);
						
      }

      else  
      {
           source = 0;
	       MPI_Bcast( &n, 1, MPI_INT, source, MPI_COMM_WORLD);
	       send = 0;
		do {
			temp = NULL;
			nacen = NULL;
			viven = NULL;
			generacion = NULL;
			MPI_Recv( intervalo, 2, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
			MPI_Bcast( &m, 1, MPI_INT, source, MPI_COMM_WORLD);
			inicio = intervalo[0];
			fin = intervalo[1];
			if (m > 0) {
				generacion = (int *) mi_malloc ((m * 2) * sizeof(int));
				MPI_Bcast( generacion, (m * 2), MPI_INT, 0, MPI_COMM_WORLD);
			}			
			if (inicio >= 0) {
				startwtime = MPI_Wtime();
				vive = nacieron = 0;
				viven = vida ( n, m, inicio, fin, &vive);
				nacen = nacer ( n, m, inicio, fin, &nacieron);
				temp = unifica ( vive, nacieron, &mtemp, viven, nacen);
				endwtime = MPI_Wtime();
				esclavo += (endwtime - startwtime);
				//MPI_Barrier ( MPI_COMM_WORLD );
				
				MPI_Send( &mtemp, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
				if (mtemp > 0) {
					MPI_Send( temp, (mtemp * 2), MPI_INT, 0, tag, MPI_COMM_WORLD);
					free ( temp );
					temp = NULL;
				}
				
				if (vive > 0) {
					free ( viven );
				}
				if (nacieron > 0) {
					free ( nacen );
				}
				free ( generacion );
			} 
			else {
				if (inicio == -2) {
					//MPI_Barrier ( MPI_COMM_WORLD );
					m = 0;
					MPI_Send( &m, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
				}
			}
			send++;	
		} while (inicio != -1);
		//MPI_Barrier ( MPI_COMM_WORLD );
		MPI_Reduce(&esclavo, &total_esclavo, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
      }

    MPI_Finalize();
    return 0;
}
/********************************************************************************/
void *mi_malloc ( int tam) {
   void *temp;

   temp = malloc (tam);

   if ( temp == NULL) {
        printf("\t\tSe ha producido un error a reservar memoria en %d en la llamada %d\n", id_proceso, llamada);
	llamada++;
        exit(0);
  }

	llamada++;
 return(temp);
}

 void leer_cells(FILE *file, int *m, int *n, int *g){

	int i, x, y;
	fscanf(file, "%d", n);
	fscanf(file, "%d", m);
	generacion = (int *) mi_malloc((*m * 2) * sizeof(int));

	for (i=0; i < (*m * 2); i+=2) {
	  fscanf(file, "%d %d", &x, &y);
	  generacion[i] = x;
	  generacion [i + 1] = y;
	}

	fscanf(file, "%d", g);	 
	fclose(file);
 }   
/*********************************************************************************
Imprime el tablero
*********************************************************************************/
void imprime (int n, int m) {
int i, j;

	for (i = 0; i < n; i++) {
		for (j = 0; j < n;j++) {

			if (esta_viva(i, j, m)) {
				fflush ( stdout );
				printf (" X ");
			} else {
				fflush ( stdout );
				printf (" _ ");
			}
		}
		printf("\n");
	}
	printf("\n\n");
}
/**********************************************************************************
Función que nos indica si una casilla está vacia o no
**********************************************************************************/
int esta_vacia (int x, int y, int m) {

	return !esta_viva (x, y, m);
}
/**********************************************************************************
Busca un célula en un vector
desde = la célula desde la que se busca
hasta = la célula que se mira, calculado mediante hasta * 2
**********************************************************************************/
int busca (int x, int y, int desde, int hasta, int *temp) {
int i;

	if (hasta == 0)
		return 0;

	for (i = (desde * 2); i < (hasta * 2); i+=2) {
		if ((temp[i] == x) && (temp[i + 1] == y)) {
			
			return 1;
		}
	}

	return 0;
}
/**********************************************************************************
Función que indica si una casilla está viva o no
***********************************************************************************/
int esta_viva (int x, int y, int m) {
int i;

	return busca (x, y, 0, m, generacion);
}
/*************************************************************************************+
Función que copia la generacion actual en una anterior
***************************************************************************************/
int *copia_gen (int size, int *gen) {
int i;
int *temp = NULL;	

	if (size > 0) {
		temp = (int *) mi_malloc((size * 2) * sizeof(int));

		for (i=0; i < (size * 2); i+=2) {
	  		temp[i] = gen[i];
	  		temp [i + 1] = gen[i + 1];
		}
	}
    return (temp);	
}
/*******************************************************************************************
Función que nos indica los vecinos vivos de una casilla
*******************************************************************************************/
int vecinos (int x, int y, int m, int n) {
int i, j, vecinos = 0;


	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			if (((x + (i-1)) >= 0) && ((x + (i-1)) < n) && ((y + (j-1)) >= 0) && ((y + (j-1)) < n)) {
				if (((x + (i-1)) != x) || ((y + (j-1)) != y)) {
					
					vecinos += esta_viva ((x + (i-1)), (y + (j-1)) , m);
					
				} else {
					//printf ("Esclavo %d envio %d Celula %d, %d Para %d, %d\n", id_proceso, send, (x+(i-1)), (y+(j-1)), x, y);
				}
			} 
		}
	}
	return vecinos;
}
/******************************************************************************************
Función que aplica la regla de muerte, es decir de cuando una célula muere si está sóla
p , tenga un vecino o más de 4.
Sólo sobreviven las células con 2 ó 3 vecinos.
*******************************************************************************************/
int *vida (int n, int m, int inicio, int hasta, int *viven) {
	int 	i,
	vecinas = 0,
	*temp = NULL;
	
	temp = (int *) mi_malloc ((2 * (hasta - inicio)) * sizeof (int));
	
	*viven = 0;

	for (i = (inicio * 2); i < (2 * hasta); i+=2) {
		vecinas = vecinos ( generacion[i], generacion[i + 1], m, n);
		
		if ((vecinas == 2) || (vecinas == 3)) {
			
				temp[(*viven * 2)] = generacion[i];
	  			temp [(*viven * 2) + 1] = generacion[i + 1];
				*viven = *viven + 1;
			 
		} else {
			//printf ("La célula %d, %d muere\n", generacion[i], generacion[i + 1]);
			
		}
	}

	return (temp);
	
}
/******************************************************************************************
Esta función aplica la norma de nacimiento, es decir, que una célula nace si una
casilla vacia tiene 3 vecinos
*******************************************************************************************/
int *nacer (int n, int m, int inicio, int hasta, int *nacen) {
	int 	k,j,i,x, y,mtemp,
	vecinas = 0,
	huecos = 0;
	int	*temp = NULL;
	mtemp = *nacen = 0;
	//printf ("Pidiendo Memoria con valor %d\n", (4 * (hasta - inicio)));
	//fflush ( stdout );
	temp = (int *) mi_malloc (500 * sizeof (int));
	for (k = (inicio * 2); k < (2 * hasta); k+=2) {
		vecinas = vecinos ( generacion[k], generacion[k + 1], m, n);
		if (vecinas > 0) { //Es una célula con opciones de crear vida
			x = generacion[k]; 
			y = generacion[k + 1];	
			for (i = 0; i < 3; i++) {
				for (j = 0; j < 3; j++) {
					if (((x + (i-1)) >= 0) && ((x + (i-1)) < n) && ((y + (j-1)) >= 0) && ((y + (j-1)) < n)) {
						if (((((x + (i-1)) %n) != x) || (((y + (j-1)) % n) != y)) && (esta_vacia (((x + (i-1)) %n),((y + (j-1)) % n),m))) {
							huecos = vecinos ( ((x + (i-1)) % n), ((y + (j-1)) % n), m, n);
							if (huecos == 3) {
								if (!busca (((x + (i-1)) %n), ((y + (j-1)) % n), 0, mtemp , temp)) {
									
									temp[(mtemp * 2)] = ((x + (i-1)) % n);
	  								temp [(mtemp * 2) + 1] = ((y + (j-1)) % n);
									mtemp = mtemp + 1;
								}
								
							}
						}
					}
				}
			}
		}
	}
	*nacen = mtemp;
	return (temp);
}


int *unifica (int nv1, int nv2, int *nfinal, int *v1, int *v2) {

	int 	n = 0,
	*temp = NULL, i, buscado;

	*nfinal = 0;
	n = nv1 + nv2;
	
	if (n > 0) {
		temp = (int *) mi_malloc ((2 * n) * sizeof (int));
	}
	for (i = 0; i < (nv1 * 2); i+=2) {
		
		buscado = busca (v1[i], v1[i+1], 0, *nfinal, temp);
		if (buscado == 0) {
			
			temp[(*nfinal * 2)] = v1[i];
	  		temp [(*nfinal * 2) + 1] = v1[i + 1];
			*nfinal = *nfinal + 1;
		}
	} 
	
	for (i = 0; i < (nv2 * 2); i+=2) {
		if (!busca (v2[i], v2[i+1], 0, *nfinal, temp)) {
			
			temp[(*nfinal * 2)] = v2[i];
	  		temp [(*nfinal * 2) + 1] = v2[i + 1];
			*nfinal = *nfinal + 1;
		}
	}
	
	return(temp);
}

int cmp (int nv1, int nv2, int *v1, int *v2) {
	int i;

	if (nv1 != nv2) {
		return 0;
	}

	for (i = 0; i < (nv1 * 2); i+=2) {
		if (!busca (v2[i], v2[i+1], 0, nv1, v1)) {
			return 0;
		}
	}

	return 1;
}
