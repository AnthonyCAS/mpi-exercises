#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
// BY Anthony CA.

//My own Broadcast program
void MPI_Broadcast(void *buffer, int count, int root, MPI_Comm comm){
    int tam,u;
    MPI_Comm_size(comm,&tam);
    for( u=1; u<tam; ++u)
        MPI_Send(buffer,count,MPI_INT,u,u,comm);
}

int main(int argc, char **argv){
    int identificador,procesos;
    //Inicio el MPI
    MPI_Init(&argc,&argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &identificador);
    MPI_Comm_size(MPI_COMM_WORLD, &procesos);

    MPI_Status estado;
    int buffer = 0 ; //contenido
    if (identificador == 0) { 
          buffer = 515;
          MPI_Broadcast(&buffer,1,0,MPI_COMM_WORLD);
    }
    printf("\nProceso: %d BEFORE tiene: %d.", identificador, buffer);
    if(identificador!=0){
      //Envio a todos los procesos con broadcast
      //MPI_Bcast(&buffer,1,MPI_INT,0,grupo1);
      //MY OWN BCAST

      MPI_Recv(&buffer,1,MPI_INT,0,MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      
    }
    printf("\nProceso: %d AFTER tiene: %d.", identificador, buffer);
  	MPI_Finalize();
    return 0;
}
