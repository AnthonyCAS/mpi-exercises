#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
// BY Anthony CA.

int main(int argc, char **argv){
    int identificador,procesos;
    //Inicio el MPI
    MPI_Init(&argc,&argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &identificador);
    MPI_Comm_size(MPI_COMM_WORLD, &procesos);

    int buffer = 0 ; //contenido
    if (identificador == 0) { 
          buffer = 515;
    }
    printf("\nProceso: %d BEFORE tiene: %d.", identificador, buffer);

    //  Envio a todos los procesos con broadcast
    MPI_Bcast(&buffer,1,MPI_INT,0,MPI_COMM_WORLD);

    printf("\nProceso: %d AFTER tiene: %d.", identificador, buffer);
  	MPI_Finalize();
    return 0;
}
