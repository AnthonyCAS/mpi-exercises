#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
//programa mpi, ping pong con dos procesos
int main(int argc, char **argv){
    int identificador,procesos;
    const int limite = 4; //limite para los pases del ping pong
    //Inicio el MPI
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &identificador);
    MPI_Comm_size(MPI_COMM_WORLD, &procesos);

    int cont = 0; //contador
    int recibe;   //variable para capturar lo que recibe
	do{
    	if (identificador == 0) { //si estoy en el primer proceso
      		cont++;
      		MPI_Wtime(); //espero apra la respuesta
      		MPI_Send(&cont, 1, MPI_INT, 1, 0, MPI_COMM_WORLD);
      		printf("Ping pong : %d envia %d a Ping pong 1.\n", identificador, cont); // envio al proceso 1
      		MPI_Wtime(); //espero la respuesta del proceso 1 y envio el incremento
      		MPI_Recv(&recibe, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); 
      		printf("Ping pong: %d recibe %d de Ping pong 1.\n", identificador, recibe);
      		cont = recibe;
    	} 
    	else {
      		//lo contrario, aqui tuve problemas , pero al cambiar el orden, de primero recuperar
      		//y luego enviar se soluciono
      		MPI_Wtime();
      		MPI_Recv(&recibe, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      		printf("Ping pong: %d recibe %d de Ping pong 0.\n", identificador, recibe);
      		cont = recibe
;      		cont++;
      		MPI_Wtime();
      		MPI_Send(&cont, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
      		printf("Ping pong : %d envia %d a Ping pong 0.\n", identificador, cont);
      		
    	}
  	}while (cont < limite);
  	MPI_Finalize();
    return 0;
}