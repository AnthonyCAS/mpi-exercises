#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
//programa mpi, ping pong con N procesos, BY Anthony CA.
//
int main(int argc, char **argv){
    int identificador,procesos;
    const int limite = 4; //limite para los pases del ping pong
    //Inicio el MPI
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &identificador);
    MPI_Comm_size(MPI_COMM_WORLD, &procesos);

    MPI_Status estado;
    int cont = 0; //contador
    int recibe,i;   //variable para capturar lo que recibe
	do{
    	if (identificador == 0) { //si estoy en el primer proceso, EL PADRE que envia a todos y recibe de todos los procesos.
      		cont++;
      	//	MPI_Wtime(); //espero apra la respuesta
            //Envio a todos los procesos el incremento
            for(i=1; i<procesos; ++i){
                MPI_Ssend(&cont, 1, MPI_INT, i, 0, MPI_COMM_WORLD);   
      		    printf("Ping pong MAIN envia %d a Ping pong: %d.\n",  cont,i); // envio al proceso 1
                
            }
      	//	MPI_Wtime(); //espero la respuesta del proceso 1 y envio el incremento
      	    for(i=1; i<procesos; ++i){
                //recibo de cualquier fuente(proceso)y cualquier etiqueta
           	    MPI_Recv(&recibe, 1, MPI_INT,MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &estado); 
      		    printf("Ping pong MAIN recibe %d de Ping pong %d.\n", recibe,estado.MPI_SOURCE);
          		cont = recibe;
        	}
        } 
    	else     {
      		//lo contrario, aqui tuve problemas , pero al cambiar el orden, de primero recuperar
      		//y luego enviar se soluciono
      	//	MPI_Wtime();
            //recivo de MAIN, tag cero
      		MPI_Recv(&recibe, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      		printf("Ping pong: %d recibe %d de Ping pong MAIN.\n", identificador, recibe);
      		cont = recibe
;      		cont++;
      	//	MPI_Wtime();
            // envio el contador a Main
      		MPI_Ssend(&cont, 1, MPI_INT, 0, identificador, MPI_COMM_WORLD);
      		printf("Ping pong: %d envia %d a Ping pong MAIN.\n", identificador, cont);
      		
    	}
  	}while (cont < limite);
  	MPI_Finalize();
    return 0;
}
