#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
//programa mpi, ping pong con N procesos, BY Anthony CA.
//
int main(int argc, char **argv){
    int identificador,procesos;
    const int limite = 4; //limite para los pases del ping pong
    //Inicio el MPI
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &identificador);
    MPI_Comm_size(MPI_COMM_WORLD, &procesos);

    int values[procesos]; //cada proceso tendra su propia valor
    MPI_Status estado;
    int recibe,i;   //variable para capturar lo que recibe
    int cont = 0;
    //inicializo el vector
    for(i=0; i<procesos; ++i)
        values[i] = i;
     
	  do{
    	if (identificador == 0) { //si estoy en el primer proceso, EL PADRE que envia a todos y recibe de todos los procesos.

            //Envio a todos los procesos valores del 1 al n-1
            for(i=1; i<procesos; ++i){
                MPI_Ssend(&values[i], 1, MPI_INT, i, 0, MPI_COMM_WORLD);   
      		      printf("Ping pong MAIN envia %d a Ping pong: %d.\n",  values[i],i); // envio al proceso 1
                
            }
      	    for(i=1; i<procesos; ++i){
                //recibo de cualquier fuente(proceso)y cualquier etiqueta
           	    MPI_Recv(&recibe, 1, MPI_INT,MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &estado);
                values[estado.MPI_SOURCE]=recibe+1; 
      		      printf("Ping pong MAIN recibe %d de Ping pong %d.\n", recibe,estado.MPI_SOURCE);     		  
        	  }  
      } 
    	else{
          //recivo de MAIN, tag cero
      		MPI_Recv(&recibe, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      		printf("Ping pong: %d recibe %d de Ping pong MAIN.\n", identificador, recibe);
      		recibe*=2;
          // envio a Main
      		MPI_Ssend(&recibe, 1, MPI_INT, 0, identificador, MPI_COMM_WORLD);
      		printf("Ping pong: %d envia %d a Ping pong MAIN.\n", identificador, recibe);
      		
    	}
      cont++;
  	}while (cont < limite);
  	MPI_Finalize();
    return 0;
}
